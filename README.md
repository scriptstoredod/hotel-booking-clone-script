our HOTEL BOOKING CLONE SCRIPT features  is an innovative alternative to online users. It’s now time to keep the papers away and make use of system storage that has easy access and lasts long. Our Script has user-friendly management settings which don’t require any programming skills. Our Clear Trip Clone Script is completely inbuilt and has automated modules which are specific for online travel business.

PRODUCT DESCRIPTION
UNIQUE FEATURES:
Ticket/Inventory availability                 
Real Time Pricing
Real Time Booking/Reservation          
Online Cancellation/Refund
Booking Amendments                          
Travel Advisory Services
Destination Management                   
White Label Solutions
Affiliate Model                                      
Multiple Payment Options
Custom booking inquiry                      
Powerful admin circle
Cross browser compatibility                 
Different ROOM BOOKING gallery
Video gallery

USER PANEL
LOGIN
Login by valid email-id and password
JavaScript and Ajax validation for email and password fields
Register link for new user
Log in using Face book and Gmail
FORGOT PASSWORD      
A password will be sent automatically to email
REGISTRATION
Basic account information such as email-id, password  and confirm password
Basic contact information such as address, mobile number  etc
After successful registration Email notification with username and password
JavaScript validation for all the required fields
Ajax validation for email-id and invalid captcha
Captcha validation
Terms and conditions view
SEARCH
Basic search by source and destination city, stay date using auto complete
View the hotel available  details
Select  the desired seat and boarding point
Enter the passenger details and send email
Ticket details send by SMS
View the hotelphotos and videos
The filter will be a sticky
Onmouse over on arrival, departure, items, pictures in tool top
WALLET
User can add cash from bank into his Wallet, so that once  booked, the cash would be generated directly from the wallet instead of going through payment gateway.
PAYMENT PAGE
Book the hotel using Payment Gateway
Validate the guest details
Ticket details send by SMS
View the terms and conditions
HOTELS
Print the hotel detail and send email
Hotel details send by Email
Hoteldetails Send by SMS
Cancel the ticket and send email
CHECK REFUND STATUS
Enter the account number and view refund status

ADMIN PANEL
LOGIN
Log in using desired username  and password  (provided by admin)
PROFILE
View the admin details
Edit the admin details, block and unblock the agent record
Change the login Password
HOTEL MANAGEMENT
Add the hotel details
Searched by hoteldetails using by  room type, from the city, to city and status
List  the hoteldetails  using on load Ajax functions
Manage the hotel details (Edit, delete, block and unblock, pagination)
View and  Block the room
View the particular hoteldetails
Structures designed by drag and drop method
HOTEL IMAGES AND VIDEOS
Add the hotel images
Manage the hotelimages(edit, status,delete)
Upload the hotelvideos
GUEST MANAGEMENT
List the booked guestdetails
View the particular guest details
View the booking details
View the particular hotel ,view the  room details
Searched by guest details using ticket no , booked date, roomtype, from city , to city
ROOM MANAGEMENT
Search and then list the room details
Searched by hoteldetails using date
HOTEL  BOOKING
Searched by hotel details using from the city, to city and date
List the hoteldetails
Select the desired hotel , trip date boarding point and then book the room
Can add/delete hotel details.
Manage bookings and guest
Manage image uploads.
Manage rooms.
Manage user info.
Maintain payment gateway and user wallet.
Manage day’s offers.
Complete end-to-end access.
CANCEL  BOOKING
List all cancel
View the particular  details
Filtered by  hotel using  book date , cancel date, travel date
PAYMENT MANAGEMENT 
List the room details
View the particularpayment transaction details
Filtered by hotel details using  type, from city, to city , date
CANCELLATION POLICIES
Add the refund status
Manage the details(edit,status,delete)
SMS LOG DETAILS
List the sms details
Manage the details(delete)
Search sms details by hotel and day,month,year
EMAIL LOG DETAILS
List the email details
Manage the details(delete)
Search details details by hoteland day,month,year

AGENT PANEL
LOGIN
Login using username and password as registered
CONTROL PANEL
Agent Profile
Deposit
Target and Incentives would be maintained
HOTEL MANAGEMENT
Agent can add the hotel details.
He can view and block the room that’s not of interest.
Manage hotel details (edit/delete/block/unblock).
ROOM MANAGEMENT
View the particular room count and details
View booker and hotel details
BOOKING REPORT
Ledger details
Detailed report
Monthly-yearly-today chart would be maintained
REPORTS
Hotel transaction reports would be maintained
WALLET DEPOSIT/PRINT /CANCEL
Amount in Wallet deposited could be reviewed
Can print or cancel hotel
GENERAL SETTINGS
Viewing and editing site settings such as site name, site URL, logo etc
JavaScript validation for all the required fields
CITY MANAGEMENT
List all the cities
Add the city
Manage the city details  (edit, status, delete, pagination)
Filtered the city using city name
LUXURY ITEM MANAGEMENT
List all the  items
Add the item
Manage the item details  (edit, status, delete, pagination)
Filtered the item using item name
USER MANAGEMENT
List of all the registered users
Manage the users details(edit,delete,status)
View the particular user details
date, user type, from city , to city
HOTEL TYPE MANAGEMENT
List of all the hotel types
Add the new hotel type
Edit, delete, activate and deactivate hotel type
HOTEL BOOKING
Searched by hotel details using from city, to city and date
List the hotel details
Select the desired hotel and boarding point and then book the  room
CANCEL HOTELS
List all cancels tickets
View the particular ticket  details
Filtered by  tickets using ticket no, cancel date, travel date
PAYMENT MANAGEMENT 
List the payment details
View the particular payment transaction details
Filtered by bus details using payment type, from the city, to city , date
COMMISSION MANAGEMENT 
List the transaction details
Calculate and view commissions
Send the payment particular service provider using Paypal
CANCELLATION POLICIES
Add the refund status
Manage the details(edit,status,delete)
HOTEL SERVICE DETAILS
Add the hotel details
Manage the details(edit,status,delete)
HOTEL BOOKING ADVANTAGE
Add the hotel booking advantages
Manage the details(edit,status,delete)
BULK SMS MANAGEMENT
Filtered the user details
Select the user and multi select the user
Send the bulk sms
SMS LOG DETAILS
List the sms details
Manage the details(delete)
Search sms details by bus and day,month,year
EMAIL LOG DETAILS
List the email details
Manage the details(delete)
Search details details by bus and day,month,year
MANAGE BANNERS
Banners to pop-up with offers would be generated.
MANAGE MARQUEE TEXT
Texts as flash notes either at the top or bottom of the page could be generated.
COUPON
Day-to-day offer coupon codes would be generated.

Check out products:
https://www.doditsolutions.com/hotel-booking-script/

http://scriptstore.in/product/hotel-booking-script/

http://phpreadymadescripts.com/airbnb-clone-script-418.html